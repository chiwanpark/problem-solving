Problem Solving
===============

이 소스코드 저장소는 Problem Solving류의 문제(KOI, IOI, ACM-ICPC, 기타 Online Judge)들을 풀고 그 결과를 저장하는 저장소 입니다. 모든 소스코드는 C++로 작성되며, STL 등을 적극적으로 사용하여 코드를 작성합니다. Online Judge 시스템이 여러개 존재하므로, 저장소의 모든 코드는 파일 이름이 "{OnlineJudgeSystem}_{ProblemID}.cpp" 형식이어야 합니다.
