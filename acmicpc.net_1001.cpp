/**
* A-B (acmicpc.net - prob. 1001)
* 2013. 03. 16.
**/

#include <iostream>

int main(int argc, const char* argv[]) {
	int a, b;
	std::cin >> a >> b;
	std::cout << a - b;
	return 0;
}