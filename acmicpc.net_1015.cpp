/*
	수열 정렬 (prob. 1015)
	acmicpc.net
	2013. 03. 24.
*/

#include <iostream>
#include <algorithm>
#include <queue>

const int MaxN = 50;
const int MaxNumber = 1000;

void inputData(void);
void outputResult(void);
void process(void);

int N;
int A[MaxN + 10], B[MaxN + 10];
int P[MaxN + 10];

std::queue<int> Queue[MaxNumber + 10];

int main(int argc, const char* argv[]) {
	inputData();
	process();
	outputResult();

	return 0;
}

void inputData(void) {
	std::cin >> N;

	for (int i = 1; i <= N; ++ i)
		std::cin >> A[i];
}

void outputResult(void) {
	for (int i = 1; i <= N; ++ i)
		std::cout << P[i] << " ";
}

void process(void) {
	for (int i = 1; i <= N; ++ i)
		B[i] = A[i];

	std::sort(B + 1, B + N + 1);

	for (int i = 1; i <= N; ++ i)
		Queue[B[i]].push(i);

	for (int i = 1; i <= N; ++ i) {
		P[i] = Queue[A[i]].front() - 1;
		Queue[A[i]].pop();
	}
}
