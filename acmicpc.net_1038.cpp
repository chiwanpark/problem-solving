/*
	감소하는 수 (prob. 1038)
	acmicpc.net
	2013. 03. 24.
*/

#include <iostream>
#include <queue>
#include <vector>
#include <functional>

const int MaxN = 1000000;

void inputData(void);
void outputResult(void);
void process(void);

int N;
long long int Answer = -1;
int Count;
std::priority_queue<long long int, std::vector<long long int>, std::greater<long long int> > Queue;

int main(int argc, const char* argv[]) {
	inputData();
	process();
	outputResult();

	return 0;
}

void inputData(void) {
	std::cin >> N;
}

void outputResult(void) {
	std::cout << Answer;
}

void process(void) {
	for (int i = 0; i <= 9; ++ i)
		Queue.push(i);

	while (Queue.empty() == false) {
		long long int v = Queue.top();
		Queue.pop();
		++ Count;

		if (Count - 1 == N) {
			Answer = v;
			return;
		}

		for (int i = 0; i <= 9; ++ i) {
			if (v % 10 > i)
				Queue.push(v * 10 + i);
		}
	}
}
