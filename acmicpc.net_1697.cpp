/**
* 캣치미이프유캔 (acmicpc.net - prob. 1697)
* 2013. 03. 16.
**/

#include <iostream>
#include <queue>
#include <climits>

const int MaxN = 105000;
const int MinN = - MaxN;
const int Shift = MaxN;

int Steps[MaxN * 2 + 10];
int N, K;
std::queue<int> Queue;

void inputData(void);
void outputResult(void);
void process(void);

int main(int argc, const char* argv[]) {
	inputData();
	process();
	outputResult();

	return 0;
}

void inputData(void) {
	std::cin >> N >> K;
}

void outputResult(void) {
	std::cout << Steps[K + Shift];
}

void process(void) {
	for (int i = 0; i <= MaxN * 2 + 1; ++ i)
		Steps[i] = INT_MAX >> 1;

	Steps[N + Shift] = 0;
	Queue.push(N);

	while(Queue.empty() == false) {
		int v = Queue.front();
		int steps = Steps[v + Shift] + 1;
		Queue.pop();

		if (MinN <= v + 1 && v + 1 <= MaxN && steps < Steps[v + 1 + Shift]) { // v + 1
			Steps[v + 1 + Shift] = steps;
			Queue.push(v + 1);
		}

		if (MinN <= v - 1 && v - 1 <= MaxN && steps < Steps[v - 1 + Shift]) { // v - 1
			Steps[v - 1 + Shift] = steps;
			Queue.push(v - 1);
		}

		if (MinN <= v * 2 && v * 2 <= MaxN && steps < Steps[v * 2 + Shift]) { // v * 2
			Steps[v * 2 + Shift] = steps;
			Queue.push(v * 2);
		}
	}
}
