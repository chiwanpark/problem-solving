#include <iostream>

void input_data(void);
void output_result(void);

int A, B;

int main(void) {
	input_data();
	output_result();

	return 0;
}

void input_data(void) {
	std::cin >> A >> B;
}

void output_result(void) {
	std::cout << A / B << std::endl << A % B;
}

