#include <iostream>

void input_data(void);
void output_result(void);

int U, H;

int main(void) {
	input_data();
	output_result();

	return 0;
}

void input_data(void) {
	std::cin >> U >> H;
}

void output_result(void) {
	std::cout << U * H / 2.0;
}

