#include <iostream>
#include <iomanip>

const double PI = 3.14;

void input_data(void);
void output_result(void);

double R;

int main(void) {
	input_data();
	output_result();

	return 0;
}

void input_data(void) {
	std::cin >> R;
}

void output_result(void) {
	std::cout << std::setprecision(2) << std::fixed << (R / 2.0 * R / 2.0 * PI);
}

