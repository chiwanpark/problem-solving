//
//  세 정수의 평균 구하기
//  koistudy, prob. 104
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    int a, b, c;
    
    scanf("%d%d%d", &a, &b, &c);
    printf("%.3lf", (a + b + c) / 3.0);
    
    return 0;
}