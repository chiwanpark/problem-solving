//
//  2차 방정식의 두 실근의 합 구하기
//  koistudy, prob. 105
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    int a, b;
    
    scanf("%d%d", &a, &b);
    printf("%lg", double(-b) / a);
    
    return 0;
}