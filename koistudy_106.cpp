//
//  두 수의 대소 비교하기
//  koistudy, prob. 106
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    long long int a, b;
    scanf("%lld%lld", &a, &b);
    printf("%lld", a > b ? a : b);
    
    return 0;
}