//
//  홀수, 짝수 판별하기
//  koistudy, prob. 107
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    long long int a;
    scanf("%lld", &a);
    printf("%s", (a % 2 == 0) ? "even" : "odd");
    
    return 0;
}