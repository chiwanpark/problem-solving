//
//  윤년 판단하기
//  koistudy, prob. 108
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    unsigned int year;
    scanf("%ud", &year);
    printf("%s", (((year % 400) == 0) || (((year % 4) == 0) && ((year % 100) != 0))) ? "Leap" : "Normal");
    
    return 0;
}