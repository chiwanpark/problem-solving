//
//  삼각형 판단하기
//  koistudy, prob. 109
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

void swap(int& a, int& b) {
    int tmp = a;
    a = b;
    b = tmp;
}

int main(int argc, const char* argv[]) {
    int a, b, c;
    
    scanf("%d%d%d", &a, &b, &c);
    
    if (a > b)
        swap(a, b);
    if (a > c)
        swap(a, c);
    if (b > c)
        swap(b, c);
    
    if (a + b <= c)
        printf("Not");
    else if (a == b && b == c)
        printf("Regular");
    else if (a * a + b * b == c * c)
        printf("Right");
    else if (a == b || b == c)
        printf("Isosceles");
    else
        printf("Normal");
    
    return 0;
}