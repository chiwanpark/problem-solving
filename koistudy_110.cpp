//
//  터널통과하기
//  koistudy, prob. 110
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    for (int i = 0; i < 3; ++ i) {
        int h;
        scanf("%d", &h);
        
        if (h <= 168) {
            printf("CRASH %d", h);
            return 0;
        }
    }
    
    printf("NO CRASH");
    
    return 0;
}