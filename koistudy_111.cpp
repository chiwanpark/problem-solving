//
//  세 정수 정렬하기
//  koistudy, prob. 111
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

void swap(int& a, int& b) {
    int tmp = a;
    a = b;
    b = tmp;
}

int main(int argc, const char* argv[]) {
    int a, b, c;
    
    scanf("%d%d%d", &a, &b, &c);
    
    if (a > b)
        swap(a, b);
    if (a > c)
        swap(a, c);
    if (b > c)
        swap(b, c);
    
    if (a == b || b == c)
        printf("Impossible");
    else
        printf("%d %d %d", a, b, c);
    
    return 0;
}