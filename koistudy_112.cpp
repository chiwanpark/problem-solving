//
//  GSHS해킹주식회사
//  koistudy, prob. 112
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

void swap(int& a, int& b) {
    int tmp = a;
    a = b;
    b = tmp;
}

int main(int argc, const char* argv[]) {
    int r, e, c;
    
    scanf("%d%d%d", &r, &e, &c);
    
    if (r < e - c)
        printf("advertise");
    else if(r > e - c)
        printf("do not advertise");
    else
        printf("does not matter");
    
    return 0;
}