//
//  2차 방정식 근 구하기
//  koistudy, prob. 113
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>
#include <math.h>

int main(int argc, const char* argv[]) {
    int a, b, c;
    
    scanf("%d%d%d", &a, &b, &c);
    
    if (b * b == 4 * a * c)
        printf("%lg", double(b) / (-2 * a));
    else if(b * b > 4 * a * c)
        printf("%lg %lg", (-b + sqrt(b * b - 4 * a * c)) / (2 * a), (-b - sqrt(b * b - 4 * a * c)) / (2 * a));
    else
        printf("none.");
    
    return 0;
}