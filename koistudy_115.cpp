//
//  1~n까지의 합 구하기
//  koistudy, prob. 115
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    int n;
    scanf("%d", &n);
    printf("%d", n * (n + 1) / 2);
    
    return 0;
}