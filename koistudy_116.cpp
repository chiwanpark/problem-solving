//
//  k의 배수의 합 출력하기
//  koistudy, prob. 116
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    int n, k, sum = 0;
    scanf("%d%d", &n, &k);
    
    for(int i = 1; i <= n; ++ i)
        sum += (i % k == 0) ? i : 0;
    
    printf("%d", sum);
    
    return 0;
}