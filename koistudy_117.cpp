//
//  0~n사이의 소수의 합 출력하기
//  koistudy, prob. 117
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

bool isPrime(int k) {
    for (int i = 2; i * i <= k; ++ i) {
        if ((k % i) == 0)
            return false;
    }
    
    return true;
}

int main(int argc, const char* argv[]) {
    int n, sum = 0;
    scanf("%d", &n);
    
    for(int i = 2; i <= n; ++ i)
        sum += isPrime(i) ? i : 0;
    
    printf("%d", sum);
    
    return 0;
}