//
//  두 정수의 최대공약수 구하기
//  koistudy, prob. 118
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int gcd(int a, int b) {
    if (a < b) {
        int tmp = a;
        a = b;
        b = tmp;
    }
    
    if (a % b == 0)
        return b;
    return gcd(b, a % b);
}

int main(int argc, const char* argv[]) {
    int a, b;
    scanf("%d%d", &a, &b);
    printf("%d", gcd(a, b));
    
    return 0;
}