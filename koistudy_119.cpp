//
//  별 그리기
//  koistudy, prob. 119
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    int n;
    
    scanf("%d", &n);
    for (int i = 0; i < 2 * n - 1; ++ i)
        printf("*");
    
    return 0;
}