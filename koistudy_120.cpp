//
//  별 그리기 2
//  koistudy, prob. 120
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    int n;
    
    scanf("%d", &n);
    for (int i = 0; i < n; ++ i) {
        for (int j = 0; j <= i; ++ j)
            printf("*");
        printf("\n");
    }
    
    return 0;
}