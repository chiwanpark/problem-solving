//
//  구구단 출력하기
//  koistudy, prob. 122
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    int n;
    
    scanf("%d", &n);
    
    if (n == 1) {
        for (int i = 1; i <= 9; ++ i) {
            for (int j = 2; j <= 9; ++ j)
                printf("%d*%d=%d ", j, i, j * i);
            printf("\n");
        }
    } else {
        for (int i = 1; i <= 9; ++ i)
            printf("%d*%d=%d\n", n, i, n * i);
    }
    
    return 0;
}