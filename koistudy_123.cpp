//
//  윷놀이
//  koistudy, prob. 123
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    int n;
    char answer[6] = "MDKGU";
    
    scanf("%d", &n);
    for (int i = 0; i < n; ++ i) {
        int sum = 0;
        for (int j = 0; j < 4; ++ j) {
            int k;
            scanf("%d", &k);
            sum += k;
        }
        
        printf("%c ", answer[sum]);
    }
    
    return 0;
}