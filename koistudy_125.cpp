//
//  3n + 1
//  koistudy, prob. 125
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <list>

void inputData(void);
void outputResult(void);
void process(void);

int A, B, Result;

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> A >> B;
    
    if (A > B) {
        int tmp = A;
        A = B;
        B = tmp;
    }
}

void outputResult(void) {
    std::cout << Result;
}

void process(void) {
    for (int i = A; i <= B; ++ i) {
        int n = i, count = 1;
        
        do {
            ++ count;
            if ((n % 2) == 0)
                n /= 2;
            else
                n = 3 * n + 1;
        } while(n != 1);
        
        if (Result < count)
            Result = count;
    }
}
