//
//  Maximum and Minimum
//  koistudy, prob. 126
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <climits>

int main(int argc, const char* argv[]) {
    int n, input, max_v = INT_MIN, max_p = 0, min_v = INT_MAX, min_p = 0;
    
    std::cin >> n;
    
    for (int i = 1; i <= n; ++ i) {
        std::cin >> input;
        
        if (max_v < input) {
            max_v = input;
            max_p = i;
        }
        if (min_v > input) {
            min_v = input;
            min_p = i;
        }
    }
    
    std::cout << max_p << " : " << max_v << std::endl << min_p << " : " << min_v;
}
