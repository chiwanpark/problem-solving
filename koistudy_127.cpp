//
//  Ancient Egyptian multiplication(a larusse Ag.)
//  koistudy, prob. 127
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int main(void) {
    int n1, n2, n3 = 0;
    
    std::cin >> n1 >> n2;
    
    do {
        if (n1 % 2)
            n3 += n2;
        
        n1 >>= 1;
        n2 <<= 1;
    } while (n1 != 0);
    
    std::cout << n3;
    
    return 0;
}
