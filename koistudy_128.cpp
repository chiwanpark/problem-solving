//
//  Number Reverse
//  koistudy, prob. 128
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <string>
#include <algorithm>

int main(void) {
    std::string input;
    
    std::cin >> input;
    std::reverse(input.begin(), input.end());
    std::cout << input;
    
    return 0;
}
