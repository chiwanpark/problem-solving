//
//  Reverse And Add
//  koistudy, prob. 129
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

void inputData(void);
void process(void);
bool isPalindrome(int);
int getReverse(int);
void outputResult(void);

int S, Sum, Count;

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> S;
}

void outputResult(void) {
    std::cout << Count << " " << Sum;
}

void process(void) {
    Sum = S;
    
    while (isPalindrome(Sum) == false) {
        Sum = Sum + getReverse(Sum);
        ++ Count;
    }
}

bool isPalindrome(int n) {
    return n == getReverse(n);
}

int getReverse(int n) {
    int result = 0;
    
    while (n >= 1) {
        result = result * 10 + n % 10;
        n /= 10;
    }
    
    return result;
}
