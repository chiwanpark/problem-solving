//
//  n번째 피보나치 수 구하기
//  koistudy, prob. 130
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int main(int argc, const char* argv[]) {
    int n;
    long long int a = 1, b = 1, c;
    
    std::cin >> n;
    
    if (n <= 2)
        std::cout << 1;
    else {
        for (int i = 3; i <= n; ++ i) {
            c = a + b;
            a = b;
            b = c;
        }
        
        std::cout << c;
    }
    
    return 0;
}
