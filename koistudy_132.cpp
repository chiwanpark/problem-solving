//
//  n명 학생 성적의 합계와 평균 구하기
//  koistudy, prob. 132
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    int n, sum = 0, input;
    
    scanf("%d", &n);
    for (int i = 1; i <= n; ++ i) {
        scanf("%d", &input);
        sum += input;
    }
    
    printf("%d\n%.2lf", sum, double(sum) / n);
    
    return 0;
}
