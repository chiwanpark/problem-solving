//
//  배열 채우기
//  koistudy, prob. 133
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int main(int argc, const char* argv[]) {
    int n;
    
    std::cin >> n;
    
    for (int i = 1; i <= n; ++ i) {
        for (int j = 1; j <= n; ++ j)
            std::cout << (i - 1) * n + j << " ";
        std::cout << std::endl;
    }
    
    return 0;
}
