//
//  배열 채우기 III
//  koistudy, prob. 135
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

const int MaxN = 100;

void inputData(void);
void process(void);
void outputResult(void);

int N;
int Board[MaxN + 10][MaxN + 10];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> N;
}

void outputResult(void) {
    for (int i = 1; i <= N; ++ i) {
        for (int j = 1; j <= N; ++ j)
            std::cout << Board[i][j] << " ";
        std::cout << std::endl;
    }
}

void process(void) {
    int x = 1, y = 1, dir = 0;
    int preset[4][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    
    for (int p = 1; p <= N * N; ++ p) {
        Board[y][x] = p;
        y += preset[dir][0];
        x += preset[dir][1];
        
        if (y > N || x > N || y < 1 || x < 1 || Board[y][x] != 0) {
            y -= preset[dir][0];
            x -= preset[dir][1];
            
            dir = (dir + 1) % 4;
            
            y += preset[dir][0];
            x += preset[dir][1];
        }
    }
}
