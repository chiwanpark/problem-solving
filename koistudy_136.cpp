//
//  Liner Structure Search
//  koistudy, prob. 136
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    int n, k, input, idx = -1;
    
    scanf("%d %d", &n, &k);
    for (int i = 1; i <= n; ++ i) {
        scanf("%d", &input);
        
        if (input == k) {
            idx = i;
            break;
        }
    }
    
    printf("%d", idx);
    
    return 0;
}
