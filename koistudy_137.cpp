//
//  O(n^2) 정렬
//  koistudy, prob. 137
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>
#include <functional>
#include <queue>

std::priority_queue<int, std::vector<int>, std::greater<int> > Queue;

int main(int argc, const char* argv[]) {
    int n, input;
    
    scanf("%d", &n);
    for (int i = 1; i <= n; ++ i) {
        scanf("%d", &input);
        Queue.push(input);
    }
    while (Queue.empty() == false) {
        input = Queue.top();
        Queue.pop();
        
        printf("%d ", input);
    }
    
    return 0;
}
