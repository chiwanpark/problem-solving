//
//  문자열 뒤집기 I
//  koistudy, prob. 139
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <cstring>

int main(int argc, const char* argv[]) {
    char input[51];
    
    std::cin.getline(input, 51);
    
    for (int i = strlen(input) - 1; i >= 0; -- i)
        std::cout << input[i];
    
    return 0;
}