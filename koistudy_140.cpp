//
//  문자열 뒤집기 II
//  koistudy, prob. 140
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <string>
#include <algorithm>

int main(int argc, const char* argv[]) {
    std::string input, temp;
    
    while (std::cin.eof() == false) {
        std::cin >> input;
        temp = "";
        for (std::string::iterator i = input.end() - 1; i != input.begin() - 1; -- i) {
            if ('!' == *i || '.' == *i)
                temp.append(i, i + 1);
            else if ('a' <= (*i) && (*i) <= 'z')
                std::cout << char((*i) - 'a' + 'A');
            else
                std::cout << char(*i);
        }
        
        std::reverse(temp.begin(), temp.end());
        std::cout << temp << " ";
    }
    
    return 0;
}
