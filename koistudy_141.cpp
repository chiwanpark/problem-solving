//
//  후위표현식 II
//  koistudy, prob. 141
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <string>
#include <stack>

void inputData(void);
void outputResult(void);
void process(void);
int getPriority(char);

std::string InFix;
std::string PostFix;

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> InFix;
}

void outputResult(void) {
    std::cout << PostFix;
}

void process(void) {
    std::stack<char> opStack;
    
    for(std::string::iterator i = InFix.begin(); i != InFix.end(); ++ i) {
        if ((*i) == '(')
            opStack.push(*i);
        else if ((*i) == ')') {
            while (opStack.top() != '(') {
                PostFix += opStack.top();
                opStack.pop();
            }
            opStack.pop();
        } else if ((*i) == '*' || (*i) == '/' || (*i) == '+' || (*i) == '-') {
            while (opStack.empty() == false && getPriority(*i) <= getPriority(opStack.top())) {
                PostFix += opStack.top();
                opStack.pop();
            }
            
            opStack.push(*i);
        } else
            PostFix += *i;
    }
    
    while (opStack.empty() == false) {
        PostFix += opStack.top();
        opStack.pop();
    }
}

int getPriority(char op) {
    if (op == '*' || op == '/')
        return 2;
    if (op == '+' || op == '-')
        return 1;
    
    return 0;
}
