//
//  O(nlog_2n) 정렬
//  koistudy, prob. 143
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>
#include <functional>
#include <list>

std::list<int> Data;

int main(int argc, const char* argv[]) {
    int n, input;
    
    scanf("%d", &n);
    for (int i = 1; i <= n; ++ i) {
        scanf("%d", &input);
        Data.push_back(input);
    }
    
    Data.sort();
    
    for (std::list<int>::iterator i = Data.begin(); i != Data.end(); ++ i)
        printf("%d ", *i);
    
    return 0;
}
