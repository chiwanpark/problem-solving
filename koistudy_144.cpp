//
//  Maximum Sum
//  koistudy, prob. 144
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>
#include <limits.h>

int main(int argc, const char* argv[]) {
    int n, sum = 0, max = INT_MIN, input;
    
    scanf("%d", &n);
    for (int i = 1; i <= n; ++ i) {
        scanf("%d", &input);
        
        if (sum > 0)
            sum = sum + input;
        else
            sum = input;
        
        if (sum > max)
            max = sum;
    }
    
    printf("%d", max);
    
    return 0;
}
