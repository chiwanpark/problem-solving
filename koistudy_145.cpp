//
//  기숙사와 파닭
//  koistudy, prob. 145
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <climits>

const int MaxN = 1000;

void inputData(void);
void outputResult(void);
void process(void);
int max(int, int);

int N;
int Dy[MaxN + 10][MaxN + 10];
int Board[MaxN + 10][MaxN + 10];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> N;
    
    for (int i = 1; i <= N; ++ i) {
        for (int j = 1; j <= N; ++ j)
            std::cin >> Board[i][j];
    }
}

void outputResult(void) {
    std::cout << Dy[N][N];
}

void process(void) {
    for (int i = 1; i <= N; ++ i) {
        Dy[0][i] = INT_MIN;
        Dy[i][0] = INT_MIN;
    }
    
    Dy[0][1] = Dy[1][0] = 0;
    for (int i = 1; i <= N; ++ i) {
        for (int j = 1; j <= N; ++ j) {
            Dy[i][j] = max(Dy[i - 1][j], Dy[i][j - 1]);
            Dy[i][j] += Board[i][j];
        }
    }
}

int max(int a, int b) {
    return a > b ? a : b;
}
