//
//  별 그리기 5
//  koistudy, prob. 149
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

void drawStar(int i, int j) {
    if (i == 0)
        return;
    if (j == 0) {
        drawStar(i - 1, i - 1);
        if (i != 1)
            std::cout << std::endl;
        return;
    }
    drawStar(i, j - 1);
    std::cout << "*";
}

int main(int argc, const char* argv[]) {
    int n;
    
    std::cin >> n;
    drawStar(n, n);
    
    return 0;
}