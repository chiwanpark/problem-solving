//
//  별 그리기 6
//  koistudy, prob. 150
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

void drawStar1(int i, int j) {
    if (i == 0)
        return;
    if (j == 0) {
        drawStar1(i - 1, i - 1);
        if (i != 1)
            std::cout << std::endl;
        return;
    }
    drawStar1(i, j - 1);
    std::cout << "*";
}

void drawStar2(int i, int j) {
    if (i == 0)
        return;
    if (j == 0) {
        if (i != 1)
            std::cout << std::endl;
        drawStar2(i - 1, i - 1);
        return;
    }
    std::cout << "*";
    drawStar2(i, j - 1);
    
}

int main(int argc, const char* argv[]) {
    int n;
    
    std::cin >> n;
    drawStar1(n, n);
    std::cout << std::endl;
    drawStar2(n - 1, n - 1);
    
    return 0;
}