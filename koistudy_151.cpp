//
//  구구단
//  koistudy, prob. 151
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int N;

void listMultiply(int i) {
    if (i == 0)
        return;
    
    listMultiply(i - 1);
    std::cout << N << "*" << i << "=" << N * i << std::endl;
}

int main(int argc, const char* argv[]) {
    std::cin >> N;
    
    listMultiply(9);
    
    return 0;
}