//
//  3n + 1
//  koistudy, prob. 152
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <list>

void inputData(void);
void outputResult(void);
int process(int);

int N;

int main(int argc, const char* argv[]) {
    inputData();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> N;
}

void outputResult(void) {
    std::cout << process(N);
}

int process(int n) {
    if (n == 1)
        return 1;
    if (n % 2)
        return process(3 * n + 1) + 1;
    return process(n / 2) + 1;
}