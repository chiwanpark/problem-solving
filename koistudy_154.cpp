//
//  피보나치 수
//  koistudy, prob. 154
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int fibo(int);

int main(int argc, const char* argv[]) {
    int n;

    std::cin >> n;
    std::cout << fibo(n);
    
    return 0;
}

int fibo(int n) {
    if (n <= 2)
        return 1;
    
    return fibo(n - 1) + fibo(n - 2);
}