//
//  피보나치 수 (중급)
//  koistudy, prob. 155
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

const int MaxN = 90;

long long int fibo(int);

long long int Dy[MaxN + 10];

int main(int argc, const char* argv[]) {
    int n;
    
    Dy[1] = Dy[2] = 1;

    std::cin >> n;
    std::cout << fibo(n);
    
    return 0;
}

long long int fibo(int n) {
    if (Dy[n] == 0)
        Dy[n] = fibo(n - 1) + fibo(n - 2);
    
    return Dy[n];
}