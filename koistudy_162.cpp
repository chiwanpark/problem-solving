//
//  대소문자변환하기
//  koistudy, prob. 162
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    char string[6];
    
    scanf("%s", string);
    for (int i = 0; i < 5; ++ i)
        printf("%c", string[i] - 'a' + 'A');
    
    return 0;
}