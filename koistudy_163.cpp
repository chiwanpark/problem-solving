//
//  암호해독
//  koistudy, prob. 163
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char* argv[]) {
    char string[9];
    int key;
    
    scanf("%d%s", &key, string);
    for (int i = 0; i < 8; ++ i)
        printf("%c", string[i] + key);
    
    return 0;
}