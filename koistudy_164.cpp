#include <cstdio>
#include <algorithm>
#include <list>

const int MaxK = 700;

void input_data(void);
void output_result(void);
void process(void);

int N, M;
int Result;
int Count[MaxK + 1];

int main(void) {
    input_data();
    process();
    output_result();

    return 0;
}

void input_data(void) {
    int k;

    scanf("%d %d", &N, &M);
    for (int i = 0; i < N; ++ i) {
        scanf("%d", &k);

        ++ Count[k];
    }
}

void output_result(void) {
    printf("%d", Result);
}

void process(void) {
    int c = 0;

    for (int i = 1; i <= MaxK; ++ i) {
        if (c + Count[i] > N - M) {
            Result += ((N - M) - c) * i;
            c = N - M;
            break;
        } else {
            c += Count[i];
            Result += i * Count[i];
        }
    }
}
