//
//  거스름돈 I
//  koistudy, prob. 166
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <climits>

const int MaxM = 10000;

void inputData(void);
void outputResult(void);
void process(void);

int N = 4, M;
int Coin[5] = {0, 500, 100, 50, 10};
int Dy[MaxM + 10];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> M;
}

void outputResult(void) {
    std::cout << Dy[M];
}

void process(void) {
    for (int i = 1; i <= M; ++ i)
        Dy[i] = INT_MAX / 2;
    
    Dy[0] = 0;
    for (int i = 1; i <= N; ++ i) {
        for (int j = 0; j <= M - Coin[i]; ++ j) {
            if (Dy[j + Coin[i]] > Dy[j] + 1)
                Dy[j + Coin[i]] = Dy[j] + 1;
        }
    }
}
