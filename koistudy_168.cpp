//
//  거스름돈 III
//  koistudy, prob. 168
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <climits>

const int MaxN = 20;
const int MaxM = 1000000;

void inputData(void);
void outputResult(void);
void process(void);
int min(int, int);

int N, M;
int Coin[MaxN + 10];
int Dy[MaxM + 10];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> M >> N;
    
    for (int i = 1; i <= N; ++ i)
        std::cin >> Coin[i];
}

void outputResult(void) {
    std::cout << Dy[M];
}

void process(void) {
    for (int i = 1; i <= M; ++ i)
        Dy[i] = INT_MAX / 2;
    
    Dy[0] = 0;
    for (int i = 1; i <= N; ++ i) {
        for (int j = 0; j <= M - Coin[i]; ++ j) {
            if (Dy[j + Coin[i]] > Dy[j] + 1)
                Dy[j + Coin[i]] = Dy[j] + 1;
        }
    }
}

int min(int a, int b) {
    return a < b ? a : b;
}
