#include <iostream>
#include <list>

const int MaxN = 100;

void input_data(void);
void output_result(void);
void process(int v); // v = number of vertex which is visited now

int N, M; // N = count of vertex, M = count of edge.
std::list<int> Edges[MaxN + 1];
int Visited[MaxN + 1];
std::list<int> Results;

int main(void) {
    input_data();
    process(1);
    output_result();

    return 0;
}

void input_data(void) {
    int s, e;

    std::cin >> N >> M;

    for (int i = 0; i < M; ++ i) {
        std::cin >> s >> e;
        Edges[s].push_back(e);
        Edges[e].push_back(s);
    }
}

void output_result(void) {
    std::list<int>::iterator end = Results.end();
    for (std::list<int>::iterator result = Results.begin(); result != end; ++ result)
        std::cout << *result << " ";
}

void process(int v) {
    if (Visited[v])
        return;

    Visited[v] = 1;
    Results.push_back(v);

    std::list<int>::iterator end = Edges[v].end();
    for (std::list<int>::iterator candidate = Edges[v].begin(); candidate != end; ++ candidate)
        process(*candidate);
}
