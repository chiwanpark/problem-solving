//
//  너비우선탐색 I
//  koistudy, prob. 171
//
//  Created by 박치완 on 13. 2. 18..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <list>
#include <queue>

const int MaxN = 100;

void inputData();
void process();
void outputResult();

int N, M;
std::list<int> Edges[MaxN + 10];
std::queue<int> Queue;
std::list<int> Result;
bool Visited[MaxN + 10];

int main(int argc, const char * argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData() {
    std::cin >> N >> M;
    
    for(int i = 0; i < M; ++ i) {
        int s, e;
        std::cin >> s >> e;
        
        Edges[s].push_back(e);
        Edges[e].push_back(s);
    }
}

void process() {
    Queue.push(1);
    
    while(Queue.empty() == false) {
        int v = Queue.front();
        Queue.pop();
        
        if (Visited[v] == true)
            continue;
        
        Visited[v] = true;
        Result.push_back(v);
        for (std::list<int>::iterator i = Edges[v].begin(); i != Edges[v].end(); ++ i) {
            if (Visited[*i] == false)
                Queue.push(*i);
        }
    }
}

void outputResult() {
    for (std::list<int>::iterator i = Result.begin(); i != Result.end(); ++ i)
        std::cout << *i << " ";
}
