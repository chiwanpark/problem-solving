//
//  확장 오각형
//  koistudy, prob. 173
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int main(int argc, const char* argv[]) {
    long long int n;
    
    std::cin >> n;
    std::cout << (3 * n * (n + 1) / 2 + (n + 1)) % 1000000004;
    
    // 5 12 22
    //  7  10
    // 3n + 1 -> 계차수열
    // Pn = 5 + sigma(3i + 1, from=2, to=n) if n >= 2
    // Pn = 5                               if n == 1
    //      3 * n * (n + 1) / 2 + n + 1     if n >= 2
    
    return 0;
}
