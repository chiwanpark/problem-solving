//
//  수열의 합
//  koistudy, prob. 174
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int main(int argc, const char* argv[]) {
    long long int n;
    
    std::cin >> n;
    std::cout << (n * (n + 1) * (2 * n + 1) / 6 + (n * (n + 1)) / 2) / 2;
    
    // An = sigma(i, 1, n) = n * (n + 1) / 2 = (n^2 + n) / 2
    // Sn = sigma(Ai, 1, n) = (n * (n + 1) * (2 * n + 1) / 6 + (n * (n + 1)) / 2) / 2
    
    return 0;
}
