//
//  수식계산 II
//  koistudy, prob. 176
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <string>
#include <stack>
#include <queue>

void inputData(void);
void outputResult(void);
void process(void);
int getPriority(char);

class Term {
public:
    char op;
    long long int operand;
    
    static Term createTerm(char op, long long int operand) {
        Term term;
        
        term.op = op;
        term.operand = operand;
        
        return term;
    }
};

std::string InFix;
long long int Result;
std::queue<Term> Expression;

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> InFix;
    InFix = "(" + InFix + ")";
}

void outputResult(void) {
    std::cout << Result;
}

void translate(void) {
    long long int operand = 0;
    std::stack<char> opStack;
    
    for(std::string::iterator i = InFix.begin(); i != InFix.end(); ++ i) {
        if ((*i) == '(')
            opStack.push(*i);
        else if ((*i) == ')') {
            if((*(i - 1)) != ')') {
                Expression.push(Term::createTerm(0, operand));
                operand = 0;
            }
            while (opStack.top() != '(') {
                Expression.push(Term::createTerm(opStack.top(), 0));
                opStack.pop();
            }
            opStack.pop();
        } else if ((*i) == '*' || (*i) == '/' || (*i) == '+' || (*i) == '-') {
            if((*(i - 1)) != ')') {
                Expression.push(Term::createTerm(0, operand));
                operand = 0;
            }
            while (opStack.empty() == false && getPriority(*i) <= getPriority(opStack.top())) {
                Expression.push(Term::createTerm(opStack.top(), 0));
                opStack.pop();
            }
            
            opStack.push(*i);
        } else
            operand = operand * 10 + ((*i) - '0');
    }
    
    while (opStack.empty() == false) {
        Expression.push(Term::createTerm(opStack.top(), 0));
        opStack.pop();
    }
}

void process(void) {
    std::stack<long long int> operandStack;
    
    translate();
    
    while (Expression.empty() == false) {
        Term term = Expression.front();
        Expression.pop();
        
        if (term.op == 0)
            operandStack.push(term.operand);
        else {
            long long int b = operandStack.top();
            operandStack.pop();
            long long int a = operandStack.top();
            operandStack.pop();
            
            if (term.op == '+')
                operandStack.push(a + b);
            else if (term.op == '-')
                operandStack.push(a - b);
            else if (term.op == '*')
                operandStack.push(a * b);
            else if (term.op == '/')
                operandStack.push(a / b);
        }
    }
    
    Result = operandStack.top();
}

int getPriority(char op) {
    if (op == '*' || op == '/')
        return 2;
    if (op == '+' || op == '-')
        return 1;
    
    return 0;
}
