#include <iostream>
#include <string>

void input_data(void);
void output_result(void);
std::string process(int);

int N;

std::string Tree;
std::string Result;

int main(void) {
	input_data();
	Result = process(1);
	output_result();

	return 0;
}

void input_data(void) {
	std::cin >> N >> Tree;
}

void output_result(void) {
	for (int i = 0, length = Result.length(); i < length; ++ i)
		std::cout << Result[i] << " ";
}

std::string process(int p) {
	std::string result = "";

	result += Tree[p - 1];

	if (p * 2 <= N)
		result += process(p * 2);

	if (p * 2 + 1 <= N)
		result += process(p * 2 + 1);

	return result;
}
