#include <cstdio>
#include <algorithm>

const int MaxN = 2000;

void input_data(void);
void output_result(void);

int N;
int Heap[MaxN + 1];

int main(void) {
    input_data();
    output_result();
}

void input_data(void) {
    scanf("%d", &N);

    for (int i = 0; i < N; ++ i) {
        scanf("%d", &Heap[i]);

        std::push_heap(Heap, Heap + i + 1);
    }
}

void output_result(void) {
    for (int i = 0; i < N; ++ i)
        printf("%d ", Heap[i]);
}
