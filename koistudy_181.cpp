//
//  영역 구분
//  koistudy, prob. 181
//
//  Created by 박치완 on 13. 2. 21..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

const int MaxN = 128;

void inputData(void);
void outputResult(void);
void process(int, int, int, int);
int getPaperType(int, int, int, int);

int N;
int Board[MaxN + 10][MaxN + 10];
int ResultW, ResultB;

int main(int argc, const char* argv[]) {
    inputData();
    process(1, 1, N, N);
    outputResult();
    
    return 0;
}

void inputData() {
    std::cin >> N;
    
    for(int i = 1; i <= N; ++ i) {
        for (int j = 1; j <= N; ++ j)
            std::cin >> Board[i][j];
    }
}

void outputResult() {
    std::cout << ResultW << std::endl << ResultB;
}

void process(int sx, int sy, int ex, int ey) {
    int paperType = getPaperType(sx, sy, ex, ey);
    
    if (paperType == 0)
        ++ ResultW;
    else if(paperType == 1)
        ++ ResultB;
    else {
        int size = (ex - sx + 1) / 2;
        
        process(sx, sy, sx + size - 1, sy + size - 1);
        process(sx + size, sy, ex, sy + size - 1);
        process(sx, sy + size, sx + size - 1, ey);
        process(sx + size, sy + size, ex, ey);
    }
}

int getPaperType(int sx, int sy, int ex, int ey) {
    bool flagB = false, flagW = false;
    
    for (int i = sy; i <= ey; ++ i) {
        for (int j = sx; j <= ex; ++ j) {
            if (Board[i][j] == 0)
                flagW = true;
            else
                flagB = true;
        }
    }
    
    if (flagB && flagW)
        return 2;
    else if (flagB)
        return 1;
    return 0;
}
