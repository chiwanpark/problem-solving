//
//  트리의 수
//  koistudy, prob. 182
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

const int MaxN = 20;

void inputData(void);
void outputResult(void);
void process();

int N;
long long int Dy[MaxN + 10];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> N;
}

void outputResult(void) {
    std::cout << Dy[N];
}

void process(void) {
    Dy[0] = 1;
    Dy[1] = 1;
    
    for (int i = 2; i <= N; ++ i) {
        for (int j = 0; j <= i - 1; ++ j)
            Dy[i] += Dy[j] * Dy[i - j - 1];
    }
}
