//
//  Combination
//  koistudy, prob. 185
//
//  Created by 박치완 on 13. 2. 21..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

const int MaxN = 30;

void inputData(void);
void outputResult(void);
void process(void);

int N, R;
long long int Dy[MaxN + 10][MaxN + 10];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData() {
    std::cin >> N >> R;
}

void outputResult() {
    std::cout << Dy[N + 1][R + 1];
}

void process() {
    Dy[0][0] = 1;
    
    for (int i = 1; i <= N + 1; ++ i) {
        for (int j = 1; j <= N + 1; ++ j)
            Dy[i][j] = Dy[i - 1][j - 1] + Dy[i - 1][j];
    }
}
