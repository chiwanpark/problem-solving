//
//  Distance of Nodes
//  koistudy, prob. 186
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <list>
#include <climits>

void inputData(void);
void outputResult(void);
void process(void);

int A, B;
int Result;

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> A >> B;
}

void outputResult(void) {
    std::cout << Result;
}

void process(void) {
    int distA, distB;
    std::list<int> AncestorA;
    std::list<int> AncestorB;
    
    for (int i = A; i >= 1; i >>= 1)
        AncestorA.push_back(i);
    for (int i = B; i >= 1; i >>= 1)
        AncestorB.push_back(i);
    
    distA = 0;
    Result = INT_MAX;
    for (std::list<int>::iterator i = AncestorA.begin(); i != AncestorA.end(); ++ i, ++ distA) {
        bool flag = false;
        distB = 0;
        for (std::list<int>::iterator j = AncestorB.begin(); j != AncestorB.end(); ++ j, ++ distB) {
            if ((*i) == (*j)) {
                flag = true;
                break;
            }
        }
        
        if (flag && Result > distA + distB)
            Result = distA + distB;
    }
}
