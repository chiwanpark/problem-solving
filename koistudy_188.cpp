//
//  약수의 합
//  koistudy, prob. 188
//
//  Created by 박치완 on 13. 2. 21..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

void inputData(void);
void outputResult(void);
void process(void);

long long int N;
long long int Result = 1;

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData() {
    std::cin >> N;
}

void outputResult() {
    std::cout << Result;
}

void process() {
    long long int prime = 2, n = N;
    
    while (n > 1) {
        long long int sum = 1;
        
        while ((n % prime) == 0) {
            n /= prime;
            sum = sum * prime + 1;
        }
        
        ++ prime;
        Result *= sum;
    }
}
