//
//  미로찾기
//  koistudy, prob. 190
//
//  Created by 박치완 on 13. 2. 21..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <cstdio>
#include <queue>
#include <climits>

const int MaxN = 4000;

struct Point {
    int y, x;
};

void inputData(void);
void outputResult(void);
void pushQueue(int y, int x, int count);
void process(void);

int H, W;
int Count[MaxN + 10][MaxN + 10];
char Board[MaxN + 10][MaxN + 10];
std::queue<Point> Queue;
Point EndPoint;

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    scanf("%d %d", &H, &W);
    
    for(int i = 1; i <= H; ++ i) {
        scanf("%s", &Board[i][1]);
        for (int j = 1; j <= W; ++ j) {
            Count[i][j] = INT_MAX >> 1;
            if (Board[i][j] == 'S')
                pushQueue(i, j, 0);
            else if(Board[i][j] == 'G') {
                EndPoint.y = i;
                EndPoint.x = j;
            }
        }
    }
}

void outputResult(void) {
    if (Count[EndPoint.y][EndPoint.x] == INT_MAX / 2)
        printf("-1");
    else
        printf("%d", Count[EndPoint.y][EndPoint.x]);
}

void pushQueue(int y, int x, int count)
{
    if (y < 1 || x < 1 || y > H || x > W || Board[y][x] == '#' || Count[y][x] <= count)
        return;
    
    Point point = {y, x};
    
    Count[y][x] = count;
    Queue.push(point);
}

void process(void) {
    int Direction[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
    
    while (Queue.empty() == false) {
        Point v = Queue.front();
        Queue.pop();
        
        if (v.y == EndPoint.y && v.x == EndPoint.x)
            return;
        
        for (int i = 0; i < 4; ++ i)
            pushQueue(v.y + Direction[i][0], v.x + Direction[i][1], Count[v.y][v.x] + 1);
    }
}
