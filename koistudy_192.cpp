//
//  수강신청
//  koistudy, prob. 192
//
//  Created by 박치완 on 13. 2. 26..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <cstdio>
#include <algorithm>

const int MaxN = 100000;

struct Lecture {
    int s, e;
    
    friend bool operator<(const Lecture& a, const Lecture& b) {
        if (a.e != b.e)
            return a.e < b.e;
        return a.s < b.s;
    }
};

void inputData(void);
void outputResult(void);
void process(void);

int N, Result;
Lecture Lectures[MaxN + 10];
int Count[MaxN + 10];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    scanf("%d", &N);
    
    for (int i = 1; i <= N; ++ i)
        scanf("%d %d", &Lectures[i].s, &Lectures[i].e);
}

void outputResult(void) {
    printf("%d", Result);
}

void process(void) {
    std::sort(Lectures + 1, Lectures + N + 1);
    
    for (int i = 1, j = 1; i <= N; ++ i) {
        Count[i] = 1;
        for (j = j; j <= N; ++ j) {
            if (Lectures[i].s > Lectures[j].e) {
                if (Count[j] + 1 > Count[i])
                    Count[i] = Count[j] + 1;
            } else
                break;
        }
        
        if (Result < Count[i])
            Result = Count[i];
    }
}
