//
//  문자열 생성
//  koistudy, prob. 194
//
//  Created by 박치완 on 13. 2. 26..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <string>

const int MaxN = 2000;

void inputData(void);
void outputResult(void);
void process(void);
void buildTString(void);

int N;
std::string S, T;
char Dy[MaxN + 10][MaxN + 10];
char Path[MaxN + 10][MaxN + 10];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> N >> S;
}

void outputResult(void) {
    std::cout << T;
}

void process(void) {
    for (int i = 0, length = S.length(); i < length; ++ i)
        Dy[i][i] = S.at(i);
    
    for (int i = 2, length = S.length(); i <= length; ++ i) {
        for (int j = 0; j + i - 1 < length; ++ j) {
            char cl = S.at(j), cr = S.at(j + i - 1);
            
            if (cl < cr) {
                Dy[j][j + i - 1] = cl;
                Path[j][j + i - 1] = -1;
            } else if (cl > cr) {
                Dy[j][j + i - 1] = cr;
                Path[j][j + i - 1] = 1;
            } else {
                Dy[j][j + i - 1] = Dy[j + 1][j + i - 2];
                Path[j][j + i - 1] = Path[j + 1][j + i - 2];
            }
        }
    }
    
    buildTString();
}

void buildTString(void) {
    int i = 0, j = S.length() - 1;
    
    while (i != j) {
        if (Path[i][j] == -1) {
            T += S.substr(i, 1);
            ++ i;
        } else {
            T += S.substr(j, 1);
            -- j;
        }
    }
    
    T += S.substr(i, 1);
}
