//
//  그래프 생성 I
//  koistudy, prob. 195
//
//  Created by 박치완 on 13. 2. 26..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int main(int argc, const char* argv[]) {
    int n;
    
    std::cin >> n;
    
    std::cout << n;
    for (int i = 1; i < n; ++ i)
        std::cout << i << " " << i + 1 << std::endl;
    std::cout << n << " 1";
    
    return 0;
}