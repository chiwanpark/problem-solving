//
//  그래프 생성 II
//  koistudy, prob. 196
//
//  Created by 박치완 on 13. 2. 26..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <utility>
#include <list>

const int MaxN = 100;

void inputData(void);
void outputResult(void);
void process(void);
int getUnion(int);
void mergeUnion(int, int);

int N, K;
int U[MaxN + 10];
std::list<std::pair<int, int> > Edges;

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> N >> K;
    
    for (int i = 1; i <= N; ++ i)
        U[i] = i;
    
    for (int i = 1; i <= K; ++ i) {
        int a, b;
        
        std::cin >> a >> b;
        mergeUnion(a, b);
    }
}

void outputResult(void) {
    std::cout << Edges.size() << std::endl;
    
    for (std::list<std::pair<int, int> >::iterator i = Edges.begin(); i != Edges.end(); ++ i) {
        std::pair<int, int> edge = *i;
        
        std::cout << edge.first << " " << edge.second << std::endl;
    }
}

void process(void) {
    for (int i = 1; i <= N; ++ i) {
        for (int j = i + 1; j <= N; ++ j) {
            int p = getUnion(i);
            int q = getUnion(j);
            
            if (p != q) {
                std::pair<int, int> edge;
                
                mergeUnion(p, q);

                edge.first = p;
                edge.second = q;
                
                Edges.push_back(edge);
            }
        }
    }
}

int getUnion(int p) {
    if (U[p] != p)
        U[p] = getUnion(U[p]);
    
    return U[p];
}

void mergeUnion(int p, int q) {
    int a = getUnion(p);
    int b = getUnion(q);
    
    if (a < b)
        U[b] = a;
    else
        U[a] = b;
}
