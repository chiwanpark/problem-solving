//
//  그래프 생성 III
//  koistudy, prob. 199
//
//  Created by 박치완 on 13. 2. 27..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <algorithm>
#include <cmath>
#include <climits>

const int MaxN = 2000;

struct Point {
    int x, y;
};

void inputData(void);
void outputResult(void);
void process(void);
long long int getDistance(const Point& a, const Point& b);

int N;
double Result;
Point Points[MaxN + 10];
long long int Distance[MaxN + 10];
bool Considered[MaxN + 10];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> N;
    
    for (int i = 1; i <= N; ++ i)
        std::cin >> Points[i].x >> Points[i].y;
}

void outputResult(void) {
    std::cout << Result;
}

void process(void) {
    for (int i = 2; i <= N + 1; ++ i)
        Distance[i] = INT_MAX >> 1;
    
    for (int i = 1; i <= N; ++ i) {
        int minPoint = N + 1;
        for (int j = 1; j <= N; ++ j) {
            if (Considered[j] == false && Distance[minPoint] > Distance[j])
                minPoint = j;
        }
        
        Result += sqrt(Distance[minPoint]);
        Considered[minPoint] = true;
        for (int j = 1; j <= N; ++ j) {
            long long int newDist = getDistance(Points[minPoint], Points[j]);
            if (Distance[j] > newDist)
                Distance[j] = newDist;
        }
    }
}

long long int getDistance(const Point& a, const Point& b) {
    long long int dx = b.x - a.x;
    long long int dy = b.y - a.y;
    
    return dx * dx + dy * dy;
}
