#include <iostream>

const int MaxN = 80000;

void input_data(void);
void output_result(void);
void process(void);

int Cow[MaxN + 10], N;
int Pos[MaxN + 10];
long long int Result;

int main(void) {
	input_data();
	process();
	output_result();

	return 0;
}

void input_data(void) {
	std::cin >> N;

	for (int i = 0; i < N; ++ i)
		std::cin >> Cow[i];
}

void output_result(void) {
	std::cout << Result;
}

void process(void) {
	for (int i = N - 1; i >= 0; -- i) {
		int count = 0;
		Pos[i] = i + 1;

		while (Pos[i] < N && Cow[Pos[i]] < Cow[i]) {
			count += Pos[Pos[i]] - Pos[i];
			Pos[i] = Pos[Pos[i]];
		}

		Result += count;
	}
}
