//
//  최대공약수
//  koistudy, prob. 203
//
//  Created by 박치완 on 13. 2. 21..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int gcd(int a, int b) {
    if (a < b) {
        int tmp = a;
        a = b;
        b = tmp;
    }
    
    if ((a % b) == 0)
        return b;
    return gcd(b, a % b);
}

int main(int argc, const char* argv[]) {
    int a, b, c;
    
    std::cin >> a >> b >> c;
    std::cout << gcd(a, gcd(b, c));
    return 0;
}
