//
//  잔디밭
//  koistudy, prob. 208
//
//  Created by 박치완 on 13. 2. 27..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

const int MaxN = 100;

int N, M, Result, Ry, Rx;
int Board[MaxN + 10][MaxN + 10];

int main(int argc, const char* argv[]) {
    std::cin >> N >> M;
    
    for (int i = 1; i <= N; ++ i) {
        for (int j = 1; j <= M; ++ j) {
            std::cin >> Board[i][j];
            Board[i][j] += Board[i - 1][j] + Board[i][j - 1] - Board[i - 1][j - 1];
        }
    }
    
    for (int i = 3; i <= N; ++ i) {
        for (int j = 3; j <= M; ++ j) {
            if (Result < Board[i][j] - Board[i - 3][j] - Board[i][j - 3] + Board[i - 3][j - 3]) {
                Result = Board[i][j] - Board[i - 3][j] - Board[i][j - 3] + Board[i - 3][j - 3];
                Rx = j - 2;
                Ry = i - 2;
            }
        }
    }
    
    std::cout << Result << std::endl << Ry << " " << Rx;
    
    return 0;
}