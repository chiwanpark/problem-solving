//
//  K-div Number
//  koistudy, prob. 225
//
//  Created by 박치완 on 13. 2. 27..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <string>
#include <cstring>

const int MaxN = 100;
const int MaxK = 100;

void inputData(void);
void outputResult(void);
void initialize(void);
void process(void);
void createDivTable(void);

int K, T;
std::string S;
int Dy[MaxN + 10][MaxK * 2 + 10];
int Div[MaxN + 10][MaxN + 10];

int main(int argc, const char* argv[]) {
    std::cin >> T;
    
    for (int i = 1; i <= T; ++ i) {
        initialize();
        inputData();
        process();
        outputResult();
    }
    
    return 0;
}

void inputData(void) {
    std::cin >> K >> S;
}

void outputResult(void) {
    std::cout << Dy[S.length()][K] << std::endl;
}

void initialize(void) {
    memset(Dy, 0, sizeof(Dy));
    memset(Div, 0, sizeof(Div));
    K = 0;
    S = "";
}

void process(void) {
    Dy[0][K] = 1;
    createDivTable();
    
    for (int i = 1, length = S.length(); i <= length; ++ i) {
        for (int j = 1; j <= i; ++ j) {
            int div = Div[j][i];
            
            for (int k = -K; k <= K; ++ k) {
                int prev = Dy[j - 1][k + K];
                
                // +
                int t = (k + div) % K + K;
                Dy[i][t] += prev;
                if (Dy[i][t] > 1000000007)
                    Dy[i][t] -= 1000000007;
                
                // -
                if (j != 1) {
                    t = (k - div) % K + K;
                    Dy[i][t] += prev;
                    if (Dy[i][t] > 1000000007)
                        Dy[i][t] -= 1000000007;
                }
            }
        }
    }
}

void createDivTable(void) {
    for (int i = 1, length = S.length(); i <= length; ++ i) {
        for (int j = i; j <= length; ++ j) {
            int n = S.at(j - 1) - '0';
            Div[i][j] = (Div[i][j - 1] * 10 + n) % K;
        }
    }
}
