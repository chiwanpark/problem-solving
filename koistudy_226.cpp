//
//  홀수의 합
//  koistudy, prob. 226
//
//  Created by 박치완 on 13. 2. 21..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int main(int argc, const char* argv[]) {
    int sum = -1;
    
    for (int i = 1; i <= 7; ++ i) {
        int input;
    
        std::cin >> input;
        
        if (input & 1)
            sum += input;
    }
    
    std::cout << (sum == -1 ? sum : sum + 1);
    
    return 0;
}
