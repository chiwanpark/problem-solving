//
//  3n + 1 (심화)
//  koistudy, prob. 231
//
//  Created by 박치완 on 13. 2. 20..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <map>

const int MaxDy = 5000000;

void inputData(void);
void outputResult(void);
void process(void);
int calcCount(int);

int A, B, Result;
int Dy[MaxDy + 10];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> A >> B;
    
    if (A > B) {
        int tmp = A;
        A = B;
        B = tmp;
    }
}

void outputResult(void) {
    std::cout << Result;
}

int calcCount(int n) {
    int count = 0, result = -1;
    long long int m = n;
    do {
        if (0 < m && m < MaxDy && Dy[m] != 0) {
            result = Dy[m] + count;
            break;
        }
        
        ++ count;
        if (m & 1)
            m = (m << 1) + m + 1;
        else
            m >>= 1;
    } while(m != 1);
    
    if (result == -1)
        result = count + 1;
    
    if (n < MaxDy)
        Dy[n] = result;
    
    return result;
}

void process(void) {
    Dy[1] = 1;
    
    for (int i = A; i <= B; ++ i) {
        if (i & 1) {
            int count = calcCount(i);
        
            if (Result < count)
                Result = count;
        }
    }
}