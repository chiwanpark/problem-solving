//
//  성적표
//  koistudy, prob. 233
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

const int MaxN = 100;

void inputData(void);
void outputResult(void);

int N;
int Score[MaxN + 10];

int main(int argc, const char* argv[]) {
    inputData();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> N;
    
    for (int i = 1; i <= N; ++ i)
        std::cin >> Score[i];
}

void outputResult(void) {
    for (int i = 1; i <= N; ++ i) {
        int rank = 1;
        
        for (int j = 1; j <= N; ++ j) {
            if (Score[i] < Score[j])
                ++ rank;
        }
        
        std::cout << Score[i] << " " << rank << std::endl;
    }
}
