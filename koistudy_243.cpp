//
//  중간값
//  koistudy, prob. 243
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <list>
#include <algorithm>

int main(int argc, const char* argv[]) {
    std::list<int> data;
    
    for (int i = 0; i < 5; ++ i) {
        int input;
        std::cin >> input;
        
        data.push_back(input);
    }
    
    data.sort();
    
    data.pop_front();
    data.pop_front();
    std::cout << data.front();
    
    return 0;
}
