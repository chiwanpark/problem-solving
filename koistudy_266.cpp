//
//  Number Triangles (숫자 삼각형)
//  koistudy, prob. 266
//
//  Created by 박치완 on 13. 2. 27..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

const int MaxN = 1000;

int N, Result;
int Board[MaxN + 10][MaxN + 10];

int max(int a, int b) {
    return a > b ? a : b;
}

int main(int argc, const char* argv[]) {
    std::cin >> N;
    
    for (int i = 1; i <= N; ++ i) {
        for (int j = 1; j <= i; ++ j) {
            std::cin >> Board[i][j];
            Board[i][j] = Board[i][j] + max(Board[i - 1][j], Board[i - 1][j - 1]);
            if (Result < Board[i][j])
                Result = Board[i][j];
        }
    }
    
    std::cout << Result;
    
    return 0;
}