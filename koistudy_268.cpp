#include <iostream>
#include <cmath>

void input_data(void);
void process(int, int);
bool is_prime(int);

int N;

int main(void) {
	input_data();
	process(0, 0);

	return 0;
}

void input_data(void) {
	std::cin >> N;
}

void process(int n, int base) {
	if (n == N)
		std::cout << base << std::endl;
	else {
		for (int i = 1; i <= 9; ++ i) {
			if (is_prime(base * 10 + i))
				process(n + 1, base * 10 + i);
		}
	}
}

bool is_prime(int n) {
	int k = sqrt(n);

	if (n == 1)
		return false;

	for(int i = 2; i <= k; ++ i) {
		if ((n % i) == 0)
			return false;
	}

	return true;
}
