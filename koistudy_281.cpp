#include <iostream>
#include <string>

void input_data(void);
void process(int, int, int, std::string);

int N;

int main(void) {
	input_data();
	process(1, 0, 1, "1");

	return 0;
}

void input_data(void) {
	std::cin >> N;
}

void process(int n, int result, int last_num, std::string output) {
	if (n == N) {
		if (result + last_num == 0)
			std::cout << output << std::endl;
	} else {
		char strNumber[3] = {' ', '0' + n + 1, 0};

		strNumber[0] = ' ';
		if (last_num > 0)
			process(n + 1, result, last_num * 10 + n + 1, output + strNumber);
		else
			process(n + 1, result, last_num * 10 - (n + 1), output + strNumber);

		strNumber[0] = '+';
		process(n + 1, result + last_num, n + 1, output + strNumber);

		strNumber[0] = '-';
		process(n + 1, result + last_num, - (n + 1), output + strNumber);
	}
}
