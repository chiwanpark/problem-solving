#include <iostream>
#include <cstring>

const int MaxN = 20;
const int MaxM = 300;

void input_data(void);
void output_result(void);
void process(void);
void find_path(int, int);

int N, M;
int Dy[MaxN + 1][MaxM + 1];
int Path[MaxN + 1][MaxM + 1];
int Profit[MaxN + 1][MaxM + 1];
int InvestProfit, InvestPortfolio[MaxN + 1];

int main(void) {
	input_data();
	process();
	output_result();

	return 0;
}

void input_data(void) {
	int invest;

	std::cin >> M >> N;

	for (int i = 1; i <= M; ++ i) {
		std::cin >> invest;
		for (int j = 1; j <= N; ++ j)
			std::cin >> Profit[j][invest];
	}
}

void output_result(void) {
	int positionOfBest = 0;

	for (int i = 0; i <= M; ++ i) {
		if (InvestProfit < Dy[N][i]) {
			InvestProfit = Dy[N][i];
			positionOfBest = i;
		}
	}

	std::cout << InvestProfit << std::endl;
	find_path(N, positionOfBest);
}

void process(void) {
	memset(Dy, -1, sizeof(Dy));

	Dy[0][0] = 0;
	for (int i = 1; i <= N; ++ i) {
		Dy[i][0] = Dy[i - 1][0];
		for (int j = 1; j <= M; ++ j) {
			Dy[i][j] = Dy[i - 1][j];
			for (int k = 0; k <= j; ++ k) {
				if (Dy[i - 1][j - k] != -1 && Dy[i][j] < Dy[i - 1][j - k] + Profit[i][k]) {
					Dy[i][j] = Dy[i - 1][j - k] + Profit[i][k];
					Path[i][j] = k;
				}
			}
		}
	}
}

void find_path(int i, int j) {
	if (i == 0)
		return;

	find_path(i - 1, j - Path[i][j]);
	std::cout << Path[i][j] << " ";
}
