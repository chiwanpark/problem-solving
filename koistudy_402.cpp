//
//  가장 큰 수
//  koistudy, prob. 402
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int main(int argc, const char* argv[]) {
    int odd = 0, even = 0;
    
    for (int i = 0; i < 7; ++ i) {
        int input;
        std::cin >> input;
        
        if ((input % 2) == 0 && input > even)
            even = input;
        if ((input % 2) != 0 && input > odd)
            odd = input;
    }
    
    std::cout << odd + even;
    
    return 0;
}
