#include <cstdio>
#include <algorithm>

const int MaxN = 10;

void input_data(void);
void output_result(void);
void process(void);
bool isAnswer(void);

int N, F;
int Candidate[MaxN];
int Triangle[MaxN];

int main(void) {
    input_data();
    process();
    output_result();

    return 0;
}

void input_data(void) {
    scanf("%d %d", &N, &F);
}

void output_result(void) {
    for (int i = 0; i < N; ++ i)
        printf("%d ", Candidate[i]);
}

void process(void) {
    for (int i = 0; i < N; ++ i) {
        Candidate[i] = i + 1;

        if (i)
            Triangle[i] = (Triangle[i - 1] * (N - i)) / i;
        else
            Triangle[i] = 1;
    }
    
    while (isAnswer() == false)
        std::next_permutation(Candidate, Candidate + N);
}

bool isAnswer(void) {
    int sum = 0;

    for (int i = 0; i < N; ++ i)
        sum += Candidate[i] * Triangle[i];

    return sum == F;
}
