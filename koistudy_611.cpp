#include <cstdio>

const int MaxN = 1000;

void input_data(void);
void output_result(void);
void process(void);
int calculate_grade(int score, int perfect);

int N;
int S[MaxN], C[MaxN], G[MaxN];

double SumOfScore;
char Grade[13][3] = {"A+", "A0", "A-", "B+", "B0", "B-", "C+", "C0", "C-", "D+", "D0", "D-", "F"};
double Point[13] = {4.3, 4.0, 3.7, 3.3, 3.0, 2.7, 2.3, 2.0, 1.7, 1.3, 1.0, 0.7, 0};
int Cutline[13] = {95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40};

int main(void) {
    input_data();
    process();
    output_result();

    return 0;
}

void input_data(void) {
    scanf("%d", &N);

    for (int i = 0; i < N; ++ i)
        scanf("%d %d", &S[i], &C[i]);
}

void output_result(void) {
    for (int i = 0; i < N; ++ i)
        printf("%s %.1lf\n", Grade[G[i]], Point[G[i]]);
    printf("%.2lf", SumOfScore / N);
}

void process(void) {
    for (int i = 0; i < N; ++ i) {
        G[i] = calculate_grade(S[i], C[i]);
        SumOfScore += Point[G[i]];
    }
}

int calculate_grade(int score, int perfect) {
    for (int i = 0; i < 13; ++ i) {
        if (score * 100 >= perfect * Cutline[i])
            return i;
    }

    return 12;
}
