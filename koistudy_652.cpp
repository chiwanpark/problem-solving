//
//  로봇
//  koistudy, prob. 652
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <queue>
#include <climits>

const int MaxN = 50;
const int MaxM = 50;

struct State {
    int x, y, direction;
};

void inputData(void);
void process(void);
int getDirection(char);
void pushState(State, int);
bool isPromising(State);
int max(int, int);
void outputResult(void);

State DirPreset[4] = {{1, 0, 0}, {0, 1, 2}, {-1, 0, 1}, {0, -1, 3}};
State TargetState;

std::queue<State> Queue;

int M, N;
int Board[MaxM + 10][MaxN + 10];
int Count[MaxM + 10][MaxN + 10][4];

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> M >> N;
    
    for (int i = 1; i <= M; ++ i) {
        for (int j = 1; j <= N; ++ j) {
            std::cin >> Board[i][j];
            for (int k = 0; k < 4; ++ k)
                Count[i][j][k] = INT_MAX;
        }
    }
    
    int x, y;
    char direction[2];
    State state;
    
    std::cin >> y >> x >> direction;
    state.y = y;
    state.x = x;
    state.direction = getDirection(direction[0]);
    pushState(state, 0);
    
    std::cin >> y >> x >> direction;
    TargetState.x = x;
    TargetState.y = y;
    TargetState.direction = getDirection(direction[0]);
}

void process(void) {
    while(Queue.empty() == false) {
        State state = Queue.front();
        Queue.pop();
        
        // rotate right
        State childState = {state.x, state.y, (state.direction + 1) % 4};
        pushState(childState, Count[state.y][state.x][state.direction] + 1);
        
        // left
        childState.direction = ((state.direction - 1) + 4) % 4;
        pushState(childState, Count[state.y][state.x][state.direction] + 1);
        
        // go
        for (int i = 1, K = max(M, N); i <= K; ++ i) {
            State childState = {
                state.x + DirPreset[state.direction].x * i,
                state.y + DirPreset[state.direction].y * i,
                state.direction
            };
            
            if (isPromising(childState))
                pushState(childState, Count[state.y][state.x][state.direction] + 1);
            else
                break;
        }
    }
}

int getDirection(char direction) {
    int num = 0;
    
    switch (direction) {
        case 'E':
            num = 0;
            break;
        case 'S':
            num = 1;
            break;
        case 'W':
            num = 2;
            break;
        case 'N':
            num = 3;
        default:
            break;
    }
    
    return num;
}

bool isPromising(State state) {
    if (state.x < 1 || state.y < 1 || state.x > N || state.y > M)
        return false;
    if (Board[state.y][state.x] == 1)
        return false;
    
    return true;
}

void pushState(State state, int count) {
    if (Count[state.y][state.x][state.direction] > count) {
        Count[state.y][state.x][state.direction] = count;
        Queue.push(state);
    }
}

int max(int a, int b) {
    return a > b ? a : b;
}

void outputResult(void) {
    std::cout << Count[TargetState.y][TargetState.x][TargetState.direction];
}
