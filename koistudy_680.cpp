//
//  합과 차
//  koistudy, prob. 680
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>

int main(int argc, const char* argv[]) {
    int n, m;
    
    std::cin >> n >> m;
    std::cout << (n + m) / 2 << std::endl << (n - m) / 2;
    return 0;
}
