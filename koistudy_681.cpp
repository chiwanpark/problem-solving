//
//  소인수분해
//  koistudy, prob. 681
//
//  Created by 박치완 on 13. 2. 19..
//  Copyright (c) 2013년 Chiwan Park. All rights reserved.
//

#include <iostream>
#include <list>

void inputData(void);
void outputResult(void);
void process(void);
int getNextPrime(int);

int N;
std::list<int> Result;

int main(int argc, const char* argv[]) {
    inputData();
    process();
    outputResult();
    
    return 0;
}

void inputData(void) {
    std::cin >> N;
}

void outputResult(void) {
    for (std::list<int>::iterator i = Result.begin(); i != Result.end(); ++ i)
        std::cout << *i << " ";
}

void process(void) {
    int prime = 2;
    
    while (N > 1) {
        while ((N % prime) == 0) {
            N /= prime;
            Result.push_back(prime);
        }
        
        prime = getNextPrime(prime);
    }
}

int getNextPrime(int prev) {
    for (int i = prev + 1; ; ++ i) {
        bool flag = false;
        for (int j = 2; j * j <= i; ++ j) {
            if ((i % j) == 0) {
                flag = true;
                break;
            }
        }
        
        if (flag == false)
            return i;
    }
    
    return -1;
}
